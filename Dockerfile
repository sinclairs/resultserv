FROM public.ecr.aws/lambda/python:3.8

COPY requirements.txt requirements.txt
RUN pip install --upgrade pip  --no-cache-dir && pip install -r requirements.txt --no-cache-dir

COPY resultserv/resultserv resultserv
COPY resultserv/decoder decoder
COPY boot.sh boot.sh
RUN chmod a+x boot.sh

EXPOSE 80:8000
#ENTRYPOINT ["./boot.sh"]
CMD ["resultserv.fastapi.handler"]
