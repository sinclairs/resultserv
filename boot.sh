#!/bin/sh
# this script is used to boot a Docker container
set -x
set -e

cd resultserv && exec uvicorn resultserv.fastapi:app --host 0.0.0.0 --port 8000
