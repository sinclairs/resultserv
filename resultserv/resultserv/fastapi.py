from fastapi import FastAPI, HTTPException, Request
from fastapi.responses import FileResponse
from mangum import Mangum

from pathlib import Path
import json

import decoder
from decoder.run_decoder import Decoder

class Settings:
    model_path = Path(decoder.__path__[0]) / 'tfmodels'

class Result:
    def __init__(self, id, model_path):
        self.id = id
        self.model_path = model_path
        self.title = self.model_path.parent.stem
        self.config = open(self.model_path.parent / 'config.json').read()
        self.state = open(self.model_path.parent / 'state.json').read()
        self.plots = sorted(self.model_path.parent.glob('*.png'))

    def asdict(self, request: Request):
        return {
            'url': request.url_for('get_result', result_id=self.id),
            'id': self.id,
            'title': self.title,
            'latest_plot': request.url_for('get_latest_plot', result_id=self.id),
            'latest_decode': request.url_for('get_latest_decode', result_id=self.id),
            'config': self.config,
            'state': self.state
        }

# Create a list of results with the last epoch of each model
result_list = [Result(i, sorted(p.parent.glob('decoder_*.keras.tf'))[-1])
               for i, p in enumerate(Settings.model_path.glob('*/decoder_000001.keras.tf'))]


###############################################################################
#   Application object                                                        #
###############################################################################

app = FastAPI()

@app.get("/")
async def root():
    return {}

###############################################################################
#   Routers configuration                                                     #
###############################################################################

@app.get("/v1/results")
async def get_results_list(request: Request):
    return [r.asdict(request) for r in result_list]

@app.get("/v1/results/{result_id}")
async def get_result(request: Request, result_id: int):
    return result_list[result_id].asdict(request)

@app.get("/v1/results/{result_id}/latest/plot")
async def get_latest_plot(result_id: int):
    return FileResponse(result_list[result_id].plots[-1], media_type='image/png')

current_decoder = None

@app.get("/v1/results/{result_id}/latest/decode")
async def get_latest_decode(result_id: int, latent: str = None):
    global current_decoder
    # FastAPI cannot handle List[List[float]] so read as string
    # see: https://github.com/tiangolo/fastapi/issues/2500
    latent = json.loads(latent)

    # Fail if not List[List[float]]
    latent = [[float(l) for l in lat] for lat in latent]

    # Load the model if necessary
    r = result_list[result_id]
    if current_decoder is None or current_decoder.path != str(r.model_path):
        current_decoder = Decoder(r.model_path.parent, 1)

    # Actually run the model here
    return current_decoder.decode(latent).tolist()

#app.include_router(results_router.router, prefix='/v1/results', tags=['results'])

###############################################################################
#   Handler for AWS Lambda                                                    #
###############################################################################

handler = Mangum(app)
